package dev.rehm.services;

import dev.rehm.models.Actor;
import dev.rehm.models.Movie;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamService {

    private List<Movie> movies;

    public StreamService(){
        super();
        this.movies = Arrays.asList(
                new Movie(1, "Gangs of New York",2002,170,
                        Arrays.asList(
                                new Actor(1, "Daniel Day-Lewis"),
                                new Actor(2, "Leonardo DiCaprio"),
                                new Actor(3, "Cameron Diaz"))),
                new Movie(2, "Titanic", 1997, 195,
                        Arrays.asList(
                                new Actor(4, "Kate Winslet"),
                                new Actor(2, "Leonardo DiCaprio"))),
                new Movie(3, "Pulp Fiction", 1994, 178,
                        Arrays.asList(
                            new Actor(5, "John Travolta"),
                            new Actor(6, "Uma Thurman"),
                            new Actor(7, "Bruce Willis"),
                            new Actor(8, "Samuel L. Jackson"))),
                new Movie(4, "Lion King", 1994, 79,
                        Arrays.asList(
                            new Actor(9, "Matthew Broderick"),
                            new Actor(10, "James Earl Jones")))
        );
    }

    /*
        Return a subset of movies that are between a minimum and maximum duration
     */
    public List<Movie> filterMovieByDuration(int max, int min){
        return movies.stream().filter(e -> e.getDuration() < max && e.getDuration() > min)
                .collect(Collectors.toList());
        //return null;
    }

    /*
        Calculate the total running time of all of the movies combined
     */
    public int calculateTotalMovieDuration(){
        return movies.stream().map(m->m.getDuration()).reduce(0, (a, b) -> a + b);
//        return movies.stream().map(Movie::getDuration).reduce(0, Integer::sum);

    }


    /*
    Find a movie with a given ID value
    */
    public Movie getMovieById(int id){
//        Optional<Movie> movieOptional = movies.stream().filter(e -> e.getId() == id).findFirst();
//        if(movieOptional.isPresent()){
//            return movieOptional.get();
//        } else {
//            return null;
//        }
        return movies.stream()
                .filter(e -> e.getId() == id)
                .findFirst().orElse(null);
        // return null;
    }

    /*
        Count the total number of Actors in all of the movies
     */
    public long getActorCount(){
//        return movies.stream().mapToLong(e -> e.getActorList().size()).sum();
        return movies.stream()
                .flatMap(m->m.getActorList().stream())
                .distinct()
                .count();
    }

    /*
        Return a map with a key of the movie name and value of comma separated Actor names
        "Titanic" "Leonardo,Kate"
     */
    public Map<String, String> getMovieActorText(){
        return movies.stream()
                .collect(Collectors.toMap(
                        m -> m.getName(),
                        m -> m.getActorList().stream().map(
                                a -> a.getName()).collect(Collectors.joining(", "))));
    }


}
