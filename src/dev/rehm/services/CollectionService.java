package dev.rehm.services;

import dev.rehm.models.Actor;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CollectionService {

    public static void main(String[] args) {
        List<Actor> actors = Arrays.asList(
                new Actor(93, "John Travolta"),
                new Actor(6, "Uma Thurman"),
                new Actor(45, "Bruce Willis"),
                new Actor(18, "Samuel L. Jackson")
        );

        Comparator<Actor> actorIdComparator = (Actor a, Actor b)->a.getId()-b.getId();

        Comparator<Actor> actorAgeComparator = (Actor a, Actor b)->{
          int birthdayComparison = a.getBirthday().compareTo(b.getBirthday());
          if(birthdayComparison==0){
              return a.getId() - b.getId();
          } else {
              return birthdayComparison;
          }
        };

        Comparator<Actor> actorNameComparator = new Comparator<Actor>() {
            @Override
            public int compare(Actor a, Actor b) {
                int nameComparison = a.getName().compareTo(b.getName());
                if(nameComparison==0){
                    return a.getId() - b.getId();
                } else {
                    return nameComparison;
                }
            }
        };

//        Collections.sort(actors,actorIdComparator);
        Collections.sort(actors);

        for(Actor a: actors){
            System.out.println(a);
        }

    }
}
